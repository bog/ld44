extends Node

const ROBOT_COUNT : int = 25
const LEVEL_COUNT : int = 3
var level_current : int = 0

class Upgrade:
	var name : String
	var cost : int
	
	func _init(n : String, c : int) -> void:
		name = n
		cost = c
		
var upgrades : Array

func _ready() -> void:
	Upgrades.player = $Player
	reset()
	

func reset():
	SoundPlayer.ladder()
	randomize()
	level_current += 1
	
	for c in $Content.get_children():
		$Content.call_deferred("remove_child", c) # WTF ??
			
	LevelGenerator.reset()	
	LevelGenerator.generate($TileMap)
	
	var rooms = LevelGenerator.rooms
	var player_start_room = rand_room()
	$Player.set_position(rand_pos_in_room(player_start_room))
	
	# Ladder
	if level_current < LEVEL_COUNT:
		var ladder : Node = preload("res://Ladder/Ladder.tscn").instance()
		ladder.position = rand_pos_in_room(rand_room_but(player_start_room))
		ladder.level = self
		$Content.add_child(ladder)
	
	for r in ROBOT_COUNT:
		var robot_room = rand_room_but(player_start_room)
		add_robot(rand_pos_in_room(robot_room))
	
	if level_current >= LEVEL_COUNT:
		var boss_room = rand_room_but(player_start_room)
		add_boss(rand_pos_in_room(boss_room))

		
func _process(delta) -> void:
	$Camera2D.position = $Player.get_position()
	process_ui()
		
	if Input.is_action_just_pressed("player_upgrade"):
		var upgrade_ui = preload("res://UpgradeUI/UpgradeUI.tscn").instance()
		upgrade_ui.player = $Player
		upgrade_ui.level = self
		get_tree().paused = true
		add_child(upgrade_ui)

func process_ui() -> void:
	$UI/VBoxContainer/LifeHBoxContainer/Life.text = str($Player/Actor.life)
	$UI/VBoxContainer/LifePotionHBoxContainer/LifePotion.text = str($Player.life_potion)
	$UI/VBoxContainer/LevelHBoxContainer/Level.text = str(level_current) + '/' + str(LEVEL_COUNT)
	
			
func rand_room_but(r):
	var room = rand_room()
	while room == r:
		room = rand_room()
	return room
			
func rand_room():
	return LevelGenerator.rooms[randi()%LevelGenerator.rooms.size()]
	
func rand_pos_in_room(room) -> Vector2:
	return Vector2(int(round(rand_range(room.j*32, room.j*32 + room.w*32 - 32))),
				 int(round(rand_range(room.i*32, room.i*32 + room.h*32 - 32))))
	
func add_robot(pos):
	var robot = preload("res://Robot/Robot.tscn").instance()
	robot.get_node("Actor").position = pos
	robot.player = $Player
	robot.level = self
	$Content.add_child(robot)

func add_boss(pos):
	var robot = preload("res://Robot/Robot.tscn").instance()
	robot.get_node("Actor").position = pos
	robot.player = $Player
	robot.level = self
	$Content.add_child(robot)
	robot.get_node("Actor/Sprite").region_rect = Rect2(Vector2(32*5, 0), Vector2(32, 32))
	robot.get_node("Actor").max_life = 300
	robot.get_node("Actor").life = robot.get_node("Actor").max_life
	robot.SHOOT_TIME = 0.3
	robot.is_boss = true