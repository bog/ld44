extends Area2D
export var tex_i : int = 0
export var tex_j : int = 0
signal took_potion

func _ready() -> void:
	$Sprite.region_rect = Rect2(Vector2(tex_j * 32, tex_i * 32), Vector2(32, 32))
	connect('body_entered', self, '_on_body_entered')
	
func _on_body_entered(body : Node) -> void:
	if body.is_in_group('Player'):
		emit_signal('took_potion')
	
