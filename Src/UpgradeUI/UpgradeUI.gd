extends CanvasLayer

var player : Node = null
var level : Node = null

func _ready() -> void:
	for upgrade in Upgrades.upgrades:
		$Panel/VBoxContainer/ItemList.add_item(upgrade.name + ' (' + str(upgrade.cost) + ')')
		

func _process(delta) ->void:
	if Input.is_action_just_pressed("player_upgrade"):
		return_game()
		
func return_game() -> void:
	get_tree().paused = false
	get_parent().remove_child(self)

func _on_ReturnButton_pressed():
	return_game()
	

func _on_ItemList_item_activated(index):
	assert player != null
	assert level != null
	var confirm : ConfirmationDialog = ConfirmationDialog.new()
	confirm.dialog_text = 'Are you sure ?\nDo you want to buy : ' + Upgrades.name(index) + ' ?'
	add_child(confirm)
	confirm.popup_centered(Vector2(256, 256))
	var btn : Button = confirm.get_ok()
	btn.connect('pressed', self, '_on_buy', [index])

func _on_buy(index):
	assert player != null
	assert level != null
	player.get_node("Actor").life -= Upgrades.cost(index)
	$Panel/VBoxContainer/ItemList.remove_item(index)
	Upgrades.apply_effect(index)
	SoundPlayer.upgrade()
	Upgrades.remove(index)
	level.process_ui()
