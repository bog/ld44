extends Area2D
var level : Node = null
func _ready() -> void:
	connect('body_entered', self, '_on_body_entered')
	
func _on_body_entered(body) -> void:
	if body.is_in_group('Player') and level != null:
		level.reset()
