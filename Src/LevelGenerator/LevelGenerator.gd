extends Node

const CELL_WIDTH : int = 50
const CELL_HEIGHT : int = CELL_WIDTH
const ROOM_MAX_WIDTH : int = 12
const ROOM_MAX_HEIGHT : int = ROOM_MAX_WIDTH
const ROOM_MIN_WIDTH : int = 5
const ROOM_MIN_HEIGHT : int = ROOM_MIN_WIDTH
const ROOM_MAX_COUNT : int = 200
var rooms : Array = []
var room_sets : Dictionary
var connecteds : Array = []
var get_room_cache : Dictionary

class Room:
	var i : int
	var j : int
	var w : int
	var h : int

class DSet:
	var parent : DSet = null
	
func dset_find(x : DSet) -> DSet:
	if x.parent == null:
		return x
	else:
		return dset_find(x.parent)

func dset_union(x : DSet, y : DSet) -> void:
	var x_root : DSet = dset_find(x)
	var y_root : DSet = dset_find(y)
	if x_root != y_root:
		x_root.parent = y_root
	 

func reset():
	rooms.clear()
	room_sets.clear()
	connecteds.clear()
	get_room_cache = {}
	for i in range(0, CELL_HEIGHT):
		get_room_cache[i] = {}
		for j in range(0, CELL_WIDTH):
			get_room_cache[i][j] = null
	

func _ready() -> void:
	reset()
	
func generate(tilemap : TileMap) -> void:
	generate_base(tilemap)
	for i in range(0, ROOM_MAX_COUNT):
		if random_room(tilemap):
			room_sets[rooms[rooms.size()-1]] = DSet.new()
	
	connect_closest_rooms(tilemap)
	
	for k in room_sets.keys():
		room_sets[k] = dset_find(room_sets[k])
	
	var dset : DSet = room_sets[room_sets.keys()[0]]
	
	for k in room_sets.keys():
		assert(room_sets[k] == dset) 
	
func connect_closest_rooms(tilemap : TileMap) -> void:
	for room in rooms:
		var closest : Room = null
		for r in rooms:
			if r != room and connecteds.find(r) == -1 and dset_find(room_sets[r]) != dset_find(room_sets[room]):
				if closest == null or abs(r.i-room.i) + abs(r.j-room.j) < abs(closest.i-room.i) + abs(closest.j-room.j):
					closest = r
		if closest != null:
			corridor(tilemap, room, closest)
			connecteds.push_back(closest)
			dset_union(room_sets[room], room_sets[closest])
			
func corridor(tilemap : TileMap, r0 : Room, r1 : Room) -> void:
	var walker_i : int = r0.i + r0.h/2
	var walker_j : int = r0.j + r0.w/2
	var target_i : int = r1.i + r1.h/2
	var target_j : int = r1.j + r1.w/2
	var end : bool = false
	
	while not end:
		tilemap.set_cell(walker_j, walker_i, 1)
		tilemap.set_cell(walker_j+1, walker_i, 1)
		tilemap.set_cell(walker_j, walker_i+1, 1)
		
		if walker_i < target_i:
			walker_i += 1
		elif walker_i > target_i:
			walker_i -= 1
		elif walker_j < target_j:
			walker_j += 1
		elif walker_j > target_j:
			walker_j -= 1
		else:
			end = true

func random_room(tilemap : TileMap) -> bool:
	var r = Room.new()
	randomize()
	r.w = int(round(rand_range(ROOM_MIN_WIDTH, ROOM_MAX_WIDTH)))
	r.h = int(round(rand_range(ROOM_MIN_HEIGHT, ROOM_MAX_HEIGHT)))
	r.i = randi()%(CELL_HEIGHT - r.h)
	r.j = randi()%(CELL_WIDTH - r.w)
	
	if not room_at(tilemap, r.i, r.j, r.w, r.h):
		for a in range(r.i, r.i + r.h):
			for b in range(r.j, r.j + r.w):
				tilemap.set_cell(b, a, 1)
		rooms.push_back(r)
		return true
	return false

func room_at(tilemap : TileMap, i : int, j : int, w : int, h : int) -> bool:
	if i <= 0 or j <= 0:
		return true
		
	for a in range(i-1, i + h+1):
		for b in range(j-1, j + w+1):
			if tilemap.get_cell(b, a) == 1:
				return true
	return false
	
func generate_base(tilemap: TileMap) -> void:
	for i in range(0, CELL_WIDTH):
		for j in range(0, CELL_HEIGHT):
			tilemap.set_cell(i, j, 0)
			
func get_room(pos : Vector2) -> Room:
	var i : int = int(pos.y/32)
	var j : int = int(pos.x/32)
	
	if get_room_cache[i][j] == null:
		for r in rooms:
			if i >= r.i and i <= r.i + r.h and j >= r.j and j <= r.j + r.w:
				get_room_cache[i][j] = r
				return r
		
		return null
	else:
		return get_room_cache[i][j]