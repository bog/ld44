extends Node

var life_potion : int = 0


var fire_dir : Vector2
var is_firing : bool = false

var fire_timer : float = 0.0

func _ready() -> void:
	pass

func _process(delta) -> void:
	$Actor.vel = Upgrades.player_vel
	if Input.is_action_just_pressed("player_quaff"):
		
		if life_potion > 0:
			SoundPlayer.quaff()
			life_potion -= 1
			$Actor.life += Upgrades.life_potion_gain
	
	process_input_fire(delta)
	
	if not $Actor.is_alive():
		get_tree().change_scene("res://LoseMenu/LoseMenu.tscn")
	
func process_input_fire(delta : float) -> void:
	
	if Input.is_action_just_pressed("fire_up"):
		is_firing = true
		fire_dir = Vector2(0, -1)
	if Input.is_action_just_pressed("fire_down"):
		is_firing = true
		fire_dir = Vector2(0, 1)
	if Input.is_action_just_pressed("fire_left"):
		is_firing = true
		fire_dir = Vector2(-1, 0)
	if Input.is_action_just_pressed("fire_right"):
		is_firing = true
		fire_dir = Vector2(1, 0)
	
	if Input.is_action_just_released("fire_up") or Input.is_action_just_released("fire_down") or Input.is_action_just_released("fire_left") or Input.is_action_just_released("fire_right") :
		is_firing = false
	
	var shoot_fct : String
	
	if Upgrades.player_weapon == Upgrades.WeaponType.Shotgun:
		shoot_fct = 'shoot_multiple'
	else:
		shoot_fct = 'shoot'
		
	if is_firing and Upgrades.player_fire_time == -1:
		$Gun.call(shoot_fct, $Actor, $Actor.position, fire_dir)	
		is_firing = false
		
	if is_firing and Upgrades.player_fire_time > 0.0:
		if fire_timer > Upgrades.player_fire_time:
			$Gun.call(shoot_fct, $Actor, $Actor.position, fire_dir)
			fire_timer = 0.0
	fire_timer += delta	

func _physics_process(delta) -> void:
	var dir : Vector2 = Vector2()

	if Input.is_action_pressed("player_up"):
		dir.y -= 1
	if Input.is_action_pressed("player_down"):
		dir.y += 1
	if Input.is_action_pressed("player_left"):
		dir.x -= 1
	if Input.is_action_pressed("player_right"):
		dir.x += 1
		
	dir = dir.normalized()
	
	$Actor.move(dir)

func get_position() -> Vector2:
	return $Actor.position
	
func set_position(pos : Vector2) -> void:
	$Actor.position = pos
	
func get_actor() -> Node:
	return $Actor
