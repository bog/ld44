extends Node

class Upgrade:
	var id : int
	var name : String
	var cost : int
	
	func _init(i : int, n : String, c : int) -> void:
		id = i
		name = n
		cost = c

var upgrades : Array
var player : Node = null

# upgradable
var bullet_damage : int = 3
var bullet_player_damage : int = 10
var player_vel : Vector2 = Vector2(200, 200)
enum WeaponType {
	Gun,
	Shotgun
}
var player_weapon : int = WeaponType.Gun
var player_fire_time : float = -1.0
var life_potion_gain : int = 20
var robot_loot_life_potion : float = 0.5
###

func _ready():
	reset()

func reset() -> void:
	bullet_damage = 3
	bullet_player_damage  = 10
	player_vel = Vector2(200, 200)
	player_weapon = WeaponType.Gun
	player_fire_time = -1.0
	life_potion_gain = 20
	robot_loot_life_potion = 0.5
	upgrades.clear()
	upgrades.push_back(Upgrade.new(0, 'More dommages !', 30))
	upgrades.push_back(Upgrade.new(1, 'Quicker !', 30))
	upgrades.push_back(Upgrade.new(2, 'Shotgun !', 50))
	upgrades.push_back(Upgrade.new(3, 'Machine gun !', 50))
	upgrades.push_back(Upgrade.new(7, 'More bullet resistance !', 70))
	upgrades.push_back(Upgrade.new(8, 'Better life potion', 30))
	upgrades.push_back(Upgrade.new(10, 'More potions !', 30))

func name(index : int) -> String:
	return upgrades[index].name

func cost(index : int) -> int:
	return upgrades[index].cost

func remove(index : int) -> void:
	assert player != null
	upgrades.remove(index)
	
func apply_effect(index : int) -> void:
	var id : int = upgrades[index].id
	
	if id == 0:
		bullet_damage = 7
		upgrades.push_back(Upgrade.new(4, 'MORE dommages I SAID!', 60))
	elif id == 1: 
		player_vel = Vector2(300, 300) 
		upgrades.push_back(Upgrade.new(5, 'Quicker quicker !!!', 70))
	elif id == 2:
		player_weapon = WeaponType.Shotgun
	elif id == 3:
		player_fire_time = 0.3
		upgrades.push_back(Upgrade.new(6, 'Mega machine gun', 100))
	elif id == 4: 
		bullet_damage = 12
	elif id == 5:
		player_vel = Vector2(500, 500)
	elif id == 6:
		player_fire_time = 0.1
	elif id == 7:
		bullet_player_damage = 5
	elif id == 8:
		life_potion_gain = 40
		upgrades.push_back(Upgrade.new(9, 'Mega life potion !!!', 150))
	elif id == 9:
		life_potion_gain = 100
	elif id == 10:
		robot_loot_life_potion = 0.8