extends Node

var life_amount : int = 1
var player : Node = null
var level : Node = null

func _ready() -> void:
	$Potion.connect('took_potion', self, 'gain_life_potion')

func gain_life_potion() -> void:
	if player != null and level != null:
		SoundPlayer.take_potion()
		player.life_potion += life_amount
		level.get_node("Content").remove_child(self)		