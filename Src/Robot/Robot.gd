extends Node
var player : Node = null
var level : Node = null

var SHOOT_TIME : float = 1.0
var shoot_timer : float = 0.0
var MIN_PLAYER_DIST : float = 64.0
var MAX_PLAYER_DIST : float = 700.0

var PLAYER_DISCOVERY_DIST : float = 300.0
var is_boss : bool = false
var player_discovered = false

func _ready() -> void:
	$Actor/Sprite.region_rect = Rect2(Vector2(128, 0), Vector2(32, 32))
	$Actor.vel = Vector2(128, 128)
	$RayCast2D.enabled = true
	$RayCast2D.add_exception($Actor)


func _process(delta) -> void:
	$Actor/LifeProgressBar.value = 100 * $Actor.life/$Actor.max_life
	
	process_death()
	process_player_move(delta)
	

func process_death() -> void:
	if not $Actor.is_alive():
		randomize()
		if rand_range(0.0, 1.0) < Upgrades.robot_loot_life_potion:
			loot()
			SoundPlayer.loot()
		if is_boss:
			get_tree().change_scene("res://WonMenu/WonMenu.tscn")
		else:
			get_parent().remove_child(self)

func process_player_move(delta) -> void:
	if player != null:
		var to_player : Vector2 = player.get_position() - $Actor.position;
		var to_player_length = to_player.length()
		$Actor/LifeProgressBar.visible = to_player_length <= MAX_PLAYER_DIST
	
		if to_player_length <= MAX_PLAYER_DIST and player_discovered:
		
			var to_player_dir : Vector2 = to_player.normalized()
			shoot_player(to_player_dir, delta)
			if to_player_length >= MIN_PLAYER_DIST and player_discovered:
				$Actor.move(to_player_dir)

func process_player_discovery() -> void:
	var to_player : Vector2 = player.get_position() - $Actor.position;
	var to_player_length = to_player.length()
		
	if player != null and not player_discovered:
	
		if to_player_length <= PLAYER_DISCOVERY_DIST and LevelGenerator.get_room($Actor.position) == LevelGenerator.get_room(player.get_position()):
			player_discovered = true
					

func _physics_process(delta) -> void:
	physics_process_player_discovery()

func physics_process_player_discovery() -> void:
	if player != null and not player_discovered:
		$RayCast2D.position = $Actor.position
		$RayCast2D.cast_to = player.get_position() - $Actor.position
		$RayCast2D.force_raycast_update()
		
		if $RayCast2D.get_collider() == player.get_node("Actor") and $RayCast2D.cast_to.length() <= PLAYER_DISCOVERY_DIST:
			player_discovered = true
								
func shoot_player(dir : Vector2, delta : float) -> void:
	if shoot_timer > SHOOT_TIME:
		shoot_timer = 0.0
		$Gun.shoot($Actor, $Actor.position, dir)
	shoot_timer += delta

func loot() -> void:
	loot_life_potion();
	
func loot_life_potion() -> void:
	var potion = preload("res://LifePotion/LifePotion.tscn").instance()
	potion.get_node("Potion").position = $Actor.position
	potion.player = player
	potion.level = level
	level.get_node("Content").add_child(potion)