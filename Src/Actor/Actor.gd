extends KinematicBody2D
export var life : int = 100
var max_life : int = 100

var vel : Vector2 = Vector2(256, 256)

func _ready() -> void:
	pass

func move(dir : Vector2) -> void:
	move_and_slide(dir * vel)

func hurt(damage : int) -> void:
	life -= damage
	
	if life < 0:
		life = 0
		
func is_alive() -> bool:
	return life > 0

