extends Node

var audio = AudioStreamPlayer.new()
var quaff : AudioStreamSample
var loot : AudioStreamSample
var upgrade : AudioStreamSample
var take_potion : AudioStreamSample
var ladder : AudioStreamSample

func _ready() -> void:
	audio.pause_mode = PAUSE_MODE_PROCESS
	add_child(audio)
	quaff = preload("res://Assets/Sounds/quaff.wav")
	loot = preload("res://Assets/Sounds/loot.wav")
	upgrade = preload("res://Assets/Sounds/upgrade.wav")
	take_potion = preload("res://Assets/Sounds/take_potion.wav")
	ladder = preload("res://Assets/Sounds/ladder.wav")

func play(a : AudioStreamSample) -> void:
	audio.stream = a
	audio.play()
	
func quaff() -> void:
	play(quaff)

func loot() -> void:
	play(loot)
	
func upgrade() -> void:
	play(upgrade)
	
func take_potion() -> void:
	play(take_potion)
	
func ladder() -> void:
	play(ladder)