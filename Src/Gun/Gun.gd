extends Node

func _ready():
	pass
	
func shoot(bullet_owner : Node, pos : Vector2, dir : Vector2) -> void:
	if get_tree() != null and get_tree().current_scene != null:
		var bullet : Node = preload("res://Bullet/Bullet.tscn").instance()
		bullet.dir = dir
		bullet.position = pos
		bullet.get_node("Sprite").rotation = atan2(dir.y, dir.x)
		bullet.bullet_owner = bullet_owner
		get_tree().current_scene.add_child(bullet)
		
func shoot_multiple(bullet_owner : Node, pos : Vector2, dir : Vector2) -> void:
	var angle = atan2(dir.y, dir.x)
	var deg_angle = rad2deg(angle)
	var i = deg_angle - 10
	while i < deg_angle + 10:
		shoot(bullet_owner, pos, Vector2(cos(deg2rad(i)), sin(deg2rad(i))))
		i += 6