extends Control

func _ready():
	pass


func _on_PlayButton_pressed():
	Upgrades.reset()
	get_tree().change_scene("res://Level/Level.tscn")


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_HTPButton_pressed():
	get_tree().change_scene("res://TutorialMenu/TutorialMenu.tscn")
