extends Area2D

var dir : Vector2 = Vector2(1, 0)
const SPEED : float = 350.0

var bullet_owner : Node = null

func _ready() -> void:
	var err : int = connect('body_entered', self, '_on_body_entered')
	assert(err == OK)
	
func _process(delta):
	position += SPEED * dir * delta

func _on_body_entered(body) -> void:
	if body != bullet_owner:
		if body.is_in_group('Hurtable'):
			if body.is_in_group('Player'):
				body.hurt(Upgrades.bullet_player_damage)
			else:
				body.hurt(Upgrades.bullet_damage)
		if self != null and get_parent() != null:
			get_parent().remove_child(self)